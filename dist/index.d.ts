export declare function time(milliseconds?: boolean): number;
export declare function date(format: string, time: number | Date): string;
