"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.date = exports.time = void 0;
function time(milliseconds) {
    if (milliseconds === void 0) { milliseconds = false; }
    var date = new Date();
    return milliseconds ? date.getTime() : Date.parse(date.toString()) / 1000;
}
exports.time = time;
function _24To12(hour) {
    return hour > 12 ? hour - 12 : hour;
}
function _zero(hour) {
    return hour > 9 ? "" + hour : "0" + hour;
}
function date(format, time) {
    var date = (typeof time === 'number') ? new Date(time) : time;
    var h12 = _24To12(date.getHours());
    var h24 = date.getHours();
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var formatMap = {
        'Y+': date.getFullYear(),
        'y+': date.getFullYear(),
        'm+': _zero(m),
        'n+': m,
        'd+': _zero(d),
        'j+': d,
        'H+': _zero(h24),
        'h+': _zero(h12),
        'g+': h12,
        'G+': h24,
        'i+': _zero(date.getMinutes()),
        's+': _zero(date.getSeconds()),
        'q+': Math.floor((date.getMonth() + 3) / 3),
        'u+': date.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in formatMap) {
        if (new RegExp('(' + k + ')').test(format)) {
            var value = formatMap[k];
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? value : (('00' + value).substr(('' + value).length)));
        }
    }
    return format;
}
exports.date = date;
