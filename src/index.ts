export function time(milliseconds = false): number {
  let date = new Date()
  return milliseconds ? date.getTime() : Date.parse(date.toString()) / 1000;
}


interface SimpleKeyValueObject {
  [key: string]: any
}

function _24To12(hour: number) {
  return hour > 12 ? hour - 12 : hour;
}

function _zero(hour: number) {
  return hour > 9 ? `${hour}` : `0${hour}`
}

export function date(format: string, time: number | Date) {
  let date = (typeof time === 'number') ? new Date(time) : time
  let h12 = _24To12(date.getHours())
  let h24 = date.getHours()
  let d = date.getDate()
  let m = date.getMonth() + 1
  let formatMap: SimpleKeyValueObject = {
    'Y+': date.getFullYear(), //4 位数字完整表示的年份
    'y+': date.getFullYear(), //2 位数字表示的年份
    'm+': _zero(m),          //数字表示的月份，有前导零
    'n+': m,                 //数字表示的月份，没有前导零
    'd+': _zero(d),                    //月份中的第几天，有前导零的 2 位数字
    'j+': d,                    //月份中的第几天，没有前导零
    'H+': _zero(h24),            //小时，24 小时格式，有前导零
    'h+': _zero(h12),   //小时，12 小时格式，有前导零
    'g+': h12,                    //小时，12 小时格式，没有前导零
    'G+': h24,                   //小时，24 小时格式，没有前导零
    'i+': _zero(date.getMinutes()),                 //有前导零的分钟数
    's+': _zero(date.getSeconds()),                 //秒数，有前导零
    'q+': Math.floor((date.getMonth() + 3) / 3), //季度
    'u+': date.getMilliseconds()             //毫秒
  };
  if (/(y+)/.test(format)) {
    format = format.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
  }
  for (let k in formatMap) {
    if (new RegExp('(' + k + ')').test(format)) {
      let value = formatMap[k]
      format = format.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? value : (('00' + value).substr(('' + value).length))
      )
    }
  }
  return format;
}

